<?php 
require_once("header.php"); 
function inputData($msdata){

    $msdata-> title="Musadilal | Contact";
    $msdata-> description="Contact description";
    $msdata-> keywords="Contact keywords";

}
?>
<script>
var element = document.getElementById("contact_act");
element.classList.add("active");
</script>

<section id="breadcrumbRow" class="row">
    <h2 style="background: url(images/common-bg1.jpg) !important;">Contact Us</h2>
    <div class="row pageTitle m0">
        <div class="container">
            <h4 class="fleft">Contact Us</h4>
            <ul class="breadcrumb fright">
                <li><a href="index.php">home</a></li>
                <li class="active">Contact</li>
            </ul>
        </div>
    </div>
</section>

<section id="contactRow" class="row contentRowPad">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="row m0">
                    <h4 class="contactHeading heading">contact form style</h4>
                </div>
                <div class="row m0 contactForm">
                    <form class="gform row m0" method="POST" data-email="code@digitalozone.in" id="contactForm"
                        action="https://script.google.com/macros/s/AKfycbyNKM4jvhFV3pXLbGM_36HRnmFwAhWh7pi2FrggjQ/exec">
                        <div class="row">
                            <div class="col-sm-6">
                                <label for="name">Name *</label>
                                <input type="text" class="form-control" name="name" id="name" pattern="^[a-zA-Z\s]{3,}$" required>
                            </div>
                            <div class="col-sm-6">
                                <label for="email">Email *</label>
                                <input type="email" class="form-control" name="email" id="email" required>
                            </div>
                        </div>
                        <div class="row m0">
                            <label for="subject">subject *</label>
                            <input type="text" class="form-control" name="subject" id="subject" required>
                        </div>
                        <div class="row m0">
                            <label for="message">your message</label>
                            <textarea name="message" id="message" class="form-control"></textarea>
                        </div>
                        <!--  <div class="row m0 captchaRow">
                                <img src="images/captcha.png" alt=""><br>
                                <label for="captcha">Enter the above text</label>
                                <input type="text" class="form-control" name="captcha" id="captcha">
                            </div> -->
                        <button class="btn btn-primary btn-lg filled" type="submit">send message</button>
                        <div style="display:none" class="f-loading">
                            <img src="loading.gif" alt="">
                        </div>
                        <div style="display:none" class="thankyou_message">
                            <!-- You can customize the thankyou message by editing the code below -->
                            <h2><em>Thanks</em> for contacting us! We will get back to you soon!
                            </h2>
                        </div>
                    </form>

                    <!-- <div id="success">
                        <span class="green textcenter">
                            Your message was sent successfully! I will be in touch as soon as I can.
                        </span>
                    </div>
                    <div id="error">
                        <span>
                            Something went wrong, try refreshing and submitting the form again.
                        </span>
                    </div> -->
                </div>
            </div>
            <div class="col-sm-6">
                <div class="row m0">
                    <h4 class="contactHeading heading">contact info style</h4>
                </div>
                <div class="media contactInfo">
                    <div class="media-left">
                        <i class="fas fa-map-marker"></i>
                    </div>
                    <div class="media-body">
                        <h5 class="heading">where to reach us</h5>
                        <p>You can reach us at the following address:</p>
                        <h5>S No 5-9-57/2,3 & 4, Basheer Bagh,<br> Hyderabad, Telangana 500029</h5>
                        <p>Branch address:</p>
                        <h5 style="color:#333">“Topaz” Amrutha Hills, Punjagutta, Hyderabad </h5>
                    </div>
                </div>
                <!--contactInfo-->
                <div class="media contactInfo">
                    <div class="media-left">
                        <i class="fas fa-envelope"></i>
                    </div>
                    <div class="media-body">
                        <h5 class="heading">Email us @</h5>
                        <p>Email your issues and suggestion for the following email addresses: </p>
                        <h5><a href="mailto:info@musaddilal.co.in">info@musaddilal.co.in</a></h5>
                    </div>
                </div>
                <!--contactInfo-->
                <div class="media contactInfo">
                    <div class="media-left">
                        <i class="fas fa-phone"></i>
                    </div>
                    <div class="media-body">
                        <h5 class="heading">need to call us?</h5>
                        <p>From Monday to Saturday,10:00 AM - 8:00 PM, call us at:</p>
                        <h5><a href="tel:9849044350">+91 9849044350</a></h5>
                        <h5><a href="tel:04023232282">040 23232282</a></h5>
                    </div>
                </div>
                <!--contactInfo-->
                <div class="media contactInfo">
                    <div class="media-left">
                        <i class="fas fa-question"></i>
                    </div>
                    <div class="media-body">
                        <h5 class="heading">we have great support</h5>
                        <p>We provide the best Quality of products to you.We are always here to help our lovely
                            customers.We ship our products anywhere with more secure. We provide the best Quality of
                            products to you.</p>
                    </div>
                </div>
                <!--contactInfo-->
            </div>
        </div>
    </div>
</section>
<section id="googleMapRow" class="row shortcodesRow" style="margin-top: 10px!important;">
    <div class="container">
        <h4 class="shortcodeHeading row">map location</h4>
    </div>
    <iframe
        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3906485.7554567493!2d78.61131201596528!3d17.027226134913363!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bcb99df819aed63%3A0xe4552bddcfc87fb!2sMusaddilals%20Jewellers%20India%20Pvt%20Ltd!5e0!3m2!1sen!2sin!4v1594818009379!5m2!1sen!2sin"
        width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false"
        tabindex="0"></iframe>
</section>



<?php require_once("footer.php"); ?>