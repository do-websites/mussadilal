<?php
require_once("header.php");
function inputData($msdata)
{
    $msdata-> title="Musadilal | Product";
    $msdata-> description="product description";
    $msdata-> keywords="product keywords";
}
?>
<script>
var element = document.getElementById("drop_act"); // only for dropdown menu items
element.classList.add("active");

/* var element = document.getElementById("diamond_act");
element.classList.add("active"); */
</script>

<section id="breadcrumbRow" class="row">
    <h2 style="background: url(images/diamond-hero.jpg) !important;">Diamond</h2>
    <div class="row pageTitle m0">
        <div class="container">
            <h4 class="fleft">diamond</h4>
            <ul class="breadcrumb fright">
                <li><a href="index.php">home</a></li>
                <li><a href="index.php">Products</a></li>
                <li class="active">diamond</li>
            </ul>
        </div>
    </div>
</section>

<section id="Musaddilal" class="row contentRowPad">
    <div class="container">
        <div class="row sectionTitle">
            <h2>Diamond Jewellery </h2>
            <h5>Our unique, custom-made diamond jewellery with antique finish are perfect for festivals, parties, and
                weddings.</h5>
        </div>
        <div class="row">
            <div class="col-sm-4 product">
                <div class="productInner row m0">
                    <div class="row m0 imgHov">
                        <img src="products/diamond/DIAMOND-PENDANT.jpg" alt="diamond">
                        <div class="row m0 hovArea">
                            <div class="row m0 proType">Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry. Lorem Ipsum has been the industry's standard dummy text ever since</div>
                        </div>
                    </div>
                    <div class="row m0 proName"><a href="#">Diamond Pendant</a></div>
                    <div class="row m0 proBuyBtn">
                        <a class="addToCart btn" href="#pqen" rel="modal:open">Product Enquiry</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 product">
                <div class="productInner row m0">
                    <div class="row m0 imgHov">
                        <img src="products/diamond/2DIAMOND-PENDANT.jpg" alt=" diamond">
                        <div class="row m0 hovArea">
                            <div class="row m0 proType">Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry. Lorem Ipsum has been the industry's standard dummy text ever since</div>
                        </div>
                    </div>
                    <div class="row m0 proName"><a href="#">Diamond Pendant</a></div>
                    <div class="row m0 proBuyBtn">
                        <a class="addToCart btn" href="#pqen" rel="modal:open">Product Enquiry</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 product">
                <div class="productInner row m0">
                    <div class="row m0 imgHov">
                        <img src="products/diamond/3-DIAMOND-PENDANT.jpg" alt="diamond">
                        <div class="row m0 hovArea">
                            <div class="row m0 proType">Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry. Lorem Ipsum has been the industry's standard dummy text ever since</div>
                        </div>
                    </div>
                    <div class="row m0 proName"><a href="#">Diamond Pendant</a></div>
                    <div class="row m0 proBuyBtn">
                        <a class="addToCart btn" href="#pqen" rel="modal:open">Product Enquiry</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 product">
                <div class="productInner row m0">
                    <div class="row m0 imgHov">
                        <img src="products/diamond/4-DIAMOND-PENDANT.jpg" alt="diamond">
                        <div class="row m0 hovArea">
                            <div class="row m0 proType">Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry. Lorem Ipsum has been the industry's standard dummy text ever since</div>
                        </div>
                    </div>
                    <div class="row m0 proName"><a href="#">Diamond Pendant</a></div>
                    <div class="row m0 proBuyBtn">
                        <a class="addToCart btn" href="#pqen" rel="modal:open">Product Enquiry</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 product">
                <div class="productInner row m0">
                    <div class="row m0 imgHov">
                        <img src="products/diamond/5DIAMOND-PENDANT.jpg" alt="diamond">
                        <div class="row m0 hovArea">

                            <div class="row m0 proType">Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry. Lorem Ipsum has been the industry's standard dummy text ever since</div>
                        </div>
                    </div>
                    <div class="row m0 proName"><a href="#">Diamond Pendant</a></div>
                    <div class="row m0 proBuyBtn">
                        <a class="addToCart btn" href="#pqen" rel="modal:open">Product Enquiry</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 product">
                <div class="productInner row m0">
                    <div class="row m0 imgHov">
                        <img src="products/diamond/6DIAMOND-PENDANT.jpg" alt="diamond">
                        <div class="row m0 hovArea">
                            <div class="row m0 proType">Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry. Lorem Ipsum has been the industry's standard dummy text ever since</div>
                        </div>
                    </div>
                    <div class="row m0 proName"><a href="#">Diamond Pendant</a></div>
                    <div class="row m0 proBuyBtn">
                        <a class="addToCart btn" href="#pqen" rel="modal:open">Product Enquiry</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 product">
                <div class="productInner row m0">
                    <div class="row m0 imgHov">
                        <img src="products/diamond/7-DIAMOND-PENDANT.jpg" alt="diamond">
                        <div class="row m0 hovArea">
                            <div class="row m0 proType">Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry. Lorem Ipsum has been the industry's standard dummy text ever since</div>
                        </div>
                    </div>
                    <div class="row m0 proName"><a href="#">Diamond Pendant</a></div>
                    <div class="row m0 proBuyBtn">
                        <a class="addToCart btn" href="#pqen" rel="modal:open">Product Enquiry</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 product">
                <div class="productInner row m0">
                    <div class="row m0 imgHov">
                        <img src="products/diamond/8-DIAMOND-PENDANT.jpg" alt="diamond">
                        <div class="row m0 hovArea">
                            <div class="row m0 proType">Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry. Lorem Ipsum has been the industry's standard dummy text ever since</div>
                        </div>
                    </div>
                    <div class="row m0 proName"><a href="#">Diamond Pendant</a></div>
                    <div class="row m0 proBuyBtn">
                        <a class="addToCart btn" href="#pqen" rel="modal:open">Product Enquiry</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 product">
                <div class="productInner row m0">
                    <div class="row m0 imgHov">
                        <img src="products/diamond/DIAMOND-CHOKER-WITH-PEARL-TASSEL-AND-EMERALD-BEADS.jpg" alt="diamond">
                        <div class="row m0 hovArea">
                            <div class="row m0 proType">Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry. Lorem Ipsum has been the industry's standard dummy text ever since</div>
                        </div>
                    </div>
                    <div class="row m0 proName"><a href="#">Diamond Choker With Pearl Tassel</a></div>
                    <div class="row m0 proBuyBtn">
                        <a class="addToCart btn" href="#pqen" rel="modal:open">Product Enquiry</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 product">
                <div class="productInner row m0">
                    <div class="row m0 imgHov">
                        <img src="products/diamond/DIAMOND-NECKLACE-WITH-CHANGEABLE-COLOR-STONES-and-PEARLS.jpg" alt="diamond">
                        <div class="row m0 hovArea">
                            <div class="row m0 proType">Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry. Lorem Ipsum has been the industry's standard dummy text ever since</div>
                        </div>
                    </div>
                    <div class="row m0 proName"><a href="#">Diamond Necklace With Changeable Color Stones & Pearls</a></div>
                    <div class="row m0 proBuyBtn">
                        <a class="addToCart btn" href="#pqen" rel="modal:open">Product Enquiry</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 product">
                <div class="productInner row m0">
                    <div class="row m0 imgHov">
                        <img src="products/diamond/DIAMOND-PENDANT-WITH-EMERALD-BEADS-AND-PEARLS.jpg" alt="diamond">
                        <div class="row m0 hovArea">
                            <div class="row m0 proType">Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry. Lorem Ipsum has been the industry's standard dummy text ever since</div>
                        </div>
                    </div>
                    <div class="row m0 proName"><a href="#">Diamond Pendant With Emerald Beads And Pearls</a></div>
                    <div class="row m0 proBuyBtn">
                        <a class="addToCart btn" href="#pqen" rel="modal:open">Product Enquiry</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 product">
                <div class="productInner row m0">
                    <div class="row m0 imgHov">
                        <img src="products/diamond/DIAMOND-SMALL-CHOKER-WITH RUBY-AND-PEARL.jpg" alt="diamond">
                        <div class="row m0 hovArea">
                            <div class="row m0 proType">Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry. Lorem Ipsum has been the industry's standard dummy text ever since</div>
                        </div>
                    </div>
                    <div class="row m0 proName"><a href="#">Diamond Small Choker With Ruby And Pearl</a></div>
                    <div class="row m0 proBuyBtn">
                        <a class="addToCart btn" href="#pqen" rel="modal:open">Product Enquiry</a>
                    </div>
                </div>
            </div>
         

        </div>
    </div>
</section>
<?php require_once("footer.php"); ?>