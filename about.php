<?php 
require_once("header.php"); 
function inputData($msdata){

    $msdata-> title="Musadilal | About us";
    $msdata-> description="about us description";
    $msdata-> keywords="about us keywords";

}
?>
<script>
var element = document.getElementById("about_act");
element.classList.add("active");
</script>
<section id="breadcrumbRow" class="row">
    <h2 style="background: url(images/common-bg1.jpg) !important;">About us</h2>
    <div class="row pageTitle m0">
        <div class="container">
            <h4 class="fleft">About us</h4>
            <ul class="breadcrumb fright">
                <li><a href="indez.php">home</a></li>
                <li class="active">about us</li>
            </ul>
        </div>
    </div>
</section>
<section id="whatWeDid" class="row contentRowPad">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 tab_menu">
                <div class="row m0">
                    <ul class="nav navbar-right" role="tablist" id="myTab">
                        <li role="presentation" class="active"><a href="#wwd2018" aria-controls="wwd2018" role="tab"
                                data-toggle="tab">1920</a></li>
                        <li role="presentation"><a href="#wwd2017" aria-controls="wwd2017" role="tab"
                                data-toggle="tab">1999</a></li>
                        <li role="presentation"><a href="#wwd2016" aria-controls="wwd2016" role="tab"
                                data-toggle="tab">2020</a></li>
                        <!--  <li role="presentation"><a href="#wwd2015" aria-controls="wwd2015" role="tab" data-toggle="tab">2015</a></li> -->
                    </ul>
                </div>
            </div>
            <div class="col-sm-6 tab-contents">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="wwd2018">
                        <h3>A Turn of the Century 1920</h3>
                        <p>Late Shri Musadddilalji along with members of the extended family set up the trade in the old
                            city of Hyderabad having gaddis/paidis in the early 1920s and subsequently by setting up
                            their retail showroom in the vicinity of Charminar.</p>
                        <p>The group is known to have had good relationships with the Royal families of Hyderabad
                            specially the Nizams and countless Nawabs. The reputable name in service and the quality was
                            known to many.</p>
                        <p>Having established a reputation, the family thought of expanding to the new city; Basheer
                            Bagh store stands for over 50 years now.</p>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="wwd2017">
                        <h3>Vision to Further Reach 1999</h3>
                        <p>This is the flagship store of the group, from where the group got known to the clients all
                            over the world. Later a store was set up in Panjagutta in 1999 with a vision to further
                            reach the ever-expanding customer base.</p>
                        <p>There has been no looking back for the firm in any aspect of the business. Each piece of
                            jewel manufactured by us goes under strict inspections and quality testing standards.</p>
                        <p>We are renowned for our unique designs and intricate workmanship each design is hand pitched
                            so that its exclusivity is maintained.</p>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="wwd2016">
                        <h3>Our Position in 2020</h3>
                        <p>We are famous for customized jewellery and we take pride in delivering every pieces on time
                            and in excellent condition. Every time a client comes with a challenge to produce a design
                            from their imagination, the sender then collects the raw material of the finest quality and
                            produce it in an atelier of high standard where every artisans are the finest in the
                            country.</p>

                    </div>
                    <!--  <div role="tabpanel" class="tab-pane" id="wwd2015">
                            <h3>What we did in 2015</h3>
                            <p>We provide the best Quality of products to you.We are always here to help our lovely customers.We ship our products anywhere with more secure.</p>
                            <p>We provide the best Quality of products to you.We are always here to help our lovely customers.We ship our products anywhere with more secure.to help our lovely customers.</p>
                            <p>We provide the best Quality of products to you.We are always here to help our lovely customers.We ship our products anywhere with more secure.to help our lovely customers.</p>
                        </div> -->
                </div>
            </div>
        </div>
    </div>
</section>
<section id="hww" class="row contentRowPad">
    <div class="container">
        <h3>COVID-19 Safe Shopping experience</h3>
        <div class="col-sm-4">
            <h5><span>01.</span> Temperature check of all<br> employees daily</h5>
            <!-- <p>We provide the best Quality of products to you.We are always here to help our lovely customers.We ship our products anywhere with more secure.</p> -->
        </div>
        <div class="col-sm-4">
            <h5><span>02.</span> Proper sanitisation and Social<br> distancing is followed</h5>

        </div>
        <div class="col-sm-4">
            <h5><span>03.</span> Aarogya Setu app<br> mandatory</h5>
        </div>
        <div class="col-sm-4">
            <h5><span>04.</span> Mask and gloves compulsory<br> to all our employees</h5>
            <!-- <p>We provide the best Quality of products to you.We are always here to help our lovely customers.We ship our products anywhere with more secure.</p> -->
        </div>
        <div class="col-sm-4">
            <h5><span>05.</span> Take 100% care about hygiene<br> and cleanlinessd</h5>

        </div>
        <div class="col-sm-4">
            <h5><span>06.</span>Store visit encouraged by<br> appointments only</h5>
        </div>
    </div>
</section>

<section id="headings" class="row" style="margin-top: 30px;">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="row m0 mb30">
                    <h1 class="heading">Customised Jewellery</h1>
                    <p class="dropcap_i dropcaFill_i"><img src="images/customised-jwellery.png" alt="icon"> Let us know
                        what you have in your mind, and we ensure to make the exact same piece of jewellery.</p>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="row m0 mb30">
                    <h2 class="heading">Restoring old Jewellery</h2>
                    <p class="dropcap_i dropcaFill_i"><img src="images/restoring-jwellery.png" alt="icon"> We restore
                        the shine of antique, old or tarnished jewellery and make it as good as new.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- <section id="ourTeam" class="row contentRowPad">
    <div class="container">
        <div class="row sectionTitle">
            <h3>meet our team</h3>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <div class="thumbnail">
                    <img src="images/team/1.png" alt="...">
                    <div class="caption">
                        <h4>Rues jack</h4>
                        <h5>Founder</h5>
                        <ul class="list-inline row m0">
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fab fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fab fa-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="thumbnail">
                    <img src="images/team/2.png" alt="...">
                    <div class="caption">
                        <h4>steve broad</h4>
                        <h5>Co-Founder</h5>
                        <ul class="list-inline row m0">
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fab fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fab fa-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="thumbnail">
                    <img src="images/team/3.png" alt="...">
                    <div class="caption">
                        <h4>olius lisa</h4>
                        <h5>Manager</h5>
                        <ul class="list-inline row m0">
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fab fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fab fa-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="thumbnail">
                    <img src="images/team/4.png" alt="...">
                    <div class="caption">
                        <h4>samuel burn</h4>
                        <h5>Employee</h5>
                        <ul class="list-inline row m0">
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fab fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fab fa-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> -->
<section id="testimonialTabs" class="row contentRowPad">
    <div class="container">
        <div class="row sectionTitle">
            <h3>some words from our customers</h3>
            <h5>A heartfelt note from our Customers Testimonials </h5>
        </div>
        <div class="row">
            <div class="col-sm-6 bbr">
                <div class="row m0 testimonialStyle2">
                    <div class="testiInner">
                        <p>“I do love jewellery designs that are unique, delicate and striking - all of which M&S's pieces are. Not to mention the quality - just gorgeous.”</p>
                        <div class="row m0 clientInfo">
                            <div class="thumbnail"><img src="images/testimonial/tm1.jpg" alt="Testimonials"></div>
                            <div class="clientName">Srijita Nandi</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 bbr">
                <div class="row m0 testimonialStyle2">
                    <div class="testiInner">
                        <p>“ Thank you, M&S - your work is just beautiful. I've had so many compliments when wearing my pendant. I just love it!”</p>
                        <div class="row m0 clientInfo">
                            <div class="thumbnail"><img src="images/testimonial/tm2.jpg" alt="Testimonials"></div>
                            <div class="clientName">Ayushi Thakur</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 bbr">
                <div class="row m0 testimonialStyle2">
                    <div class="testiInner">
                        <p>“ he pieces are exactly as they appear on the website, contemporary with a nod to classic design and the quality is of a high standard ... Love it! ”</p>
                        <div class="row m0 clientInfo">
                            <div class="thumbnail"><img src="images/testimonial/tm3.jpg" alt="Testimonials"></div>
                            <div class="clientName">Amrita Reddy</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 bbr">
                <div class="row m0 testimonialStyle2">
                    <div class="testiInner">
                        <p>“ I feel lucky to have found the delicate, unique and special necklaces and earrings that I have purchased from Musaddilal.”</p>
                        <div class="row m0 clientInfo">
                            <div class="thumbnail"><img src="images/testimonial/tm4.jpg" alt="Testimonials"></div>
                            <div class="clientName">Trishna</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Testimonial Tabs-->
<!-- <section id="brands" class="row contentRowPad">
    <div class="container">
        <div class="row sectionTitle">
            <h3>our clients</h3>
            <h5>clients who are happy with our Dshine</h5>
        </div>
        <div class="row brands">
            <ul class="nav navbar-nav">
                <li>
                    <a href="#"><img src="images/clients/1.png" alt=""></a>
                </li>
                <li>
                    <a href="#"><img src="images/clients/2.png" alt=""></a>
                </li>
                <li>
                    <a href="#"><img src="images/clients/3.png" alt=""></a>
                </li>
                <li>
                    <a href="#"><img src="images/clients/4.png" alt=""></a>
                </li>
                <li>
                    <a href="#"><img src="images/clients/5.png" alt=""></a>
                </li>
                <li>
                    <a href="#"><img src="images/clients/2.png" alt=""></a>
                </li>
            </ul>
        </div>
    </div>
</section> -->
<?php require_once("footer.php"); ?>