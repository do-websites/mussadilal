<footer class="row">
    <div class="row m0 topFooter">
        <!-- <div class="container line1">
            <div class="row footFeatures">
                <div class="col-sm-4 footFeature">
                    <div class="media">
                        <div class="media-left icon"><img src="images/icons/car3.png" alt=""></div>
                        <div class="media-body texts">
                            <h4>free shipping</h4>
                            <h2>International</h2>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 footFeature">
                    <div class="media m0">
                        <div class="media-left icon"><img src="images/icons/tel24-7_2.png" alt=""></div>
                        <div class="media-body texts">
                            <h4>24 hours helpline</h4>
                            <h2>123 456 789</h2>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 footFeature">
                    <div class="media m0">
                        <div class="media-left icon"><img src="images/icons/shopping-bag2.png" alt=""></div>
                        <div class="media-body texts">
                            <h4>see our</h4>
                            <h2>latest offers</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="container line2">
            <div class="row">
                <div class="col-sm-3 widget">
                    <div class="row m0">
                        <h4>About mussadilal</h4>
                        <p>Musaddilal brings the finest of gems and jewellery for you to adorn yourself.</p>
                        <ul class="list-inline">
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fab fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fab fa-dribbble"></i></a></li>
                            <li><a href="#"><i class="fab fa-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-3 widget">
                    <div class="row m0">
                        <h4>quick links</h4>
                        <ul class="nav">
                            <li><a href="index.php">Home</a></li>
                            <li><a href="about.php">About Us</a></li>
                            <li><a href="blogs.php">Blog</a></li>
                            <li><a href="contact.php">Contact Us</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-3 widget">
                    <div class="row m0">
                        <h4>top products</h4>
                        <ul class="nav">
                            <li><a href="product-diamond.php">Diamond</a></li>
                            <li><a href="product-kundan.php">Kundan</a></li>
                            <li><a href="product-gold.php">Gold</a></li>
                            <li><a href="product-polki.php">Polki</a></li>

                        </ul>
                    </div>
                </div>
                <div class="col-sm-3 widget">
                    <div class="row m0">
                        <h4>subscribe to our latest news</h4>
                        <form action="#" method="post" role="form">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fas fa-envelope"></i></span>
                                <input type="email" class="form-control" id="subscribeEmail" name="subscribeEmail"
                                    placeholder="EMAIL ADDRESS">
                            </div>
                            <input type="submit" class="form-control" value="Subscribe" id="submit" name="submit">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row m0 copyRight">
        <div class="container">
            <div class="row">
                <div class="fleft">&copy; 2020 <a href="index.php">Musaddilal</a> All Rights Reserved. Developed by
                    Digitalozone</div>

            </div>
        </div>
    </div>
    
</footer>
<!--jQuery, Bootstrap and other vendor JS-->
<!--jQuery-->
<script src="js/jquery-2.1.3.min.js"></script>
<!--Google Maps-->
<script src="https://maps.googleapis.com/maps/api/js"></script>
<!--Bootstrap JS-->
<script src="js/bootstrap.min.js"></script>

<!--Owl Carousel-->
<script src="vendors/owl.carousel/js/owl.carousel.min.js"></script>
<!--Isotope-->
<script src="vendors/isotope/isotope-custom.js"></script>
<!--FlexSlider-->
<script src="vendors/flexslider/jquery.flexslider-min.js"></script>

<!--Musaddilal JS-->
<script src="js/google-app-script.js"></script>
<script src="js/d-shine.js"></script>
<script>
setTimeout(function(){
  $('body').addClass('loaded');
}, 2000);
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>

<script src="https://www.appointletcdn.com/loader/loader.min.js" async="" defer=""></script>


<!-- model product enquary form -->
<div id="pqen" class="modal">
    <form class="gform row m0" id="product-q" method="POST" data-email="code@digitalozone.in" action="https://script.google.com/macros/s/AKfycbxulOUPsU3rQYwmxlJNUY1bs7EOgfhI8tjQv0P6/exec">
        <div class="row p-t-20">
            <div class="col-sm-6">
                <label for="name">Name *</label>
                <input type="text" class="form-control" name="name" id="name" pattern="^[a-zA-Z\s]{3,}$" required>
            </div>
            <div class="col-sm-6 m-t-12">
                <label for="email">Email *</label>
                <input type="email" class="form-control" name="email" id="email" required>
            </div>
        </div>
        <div class="row p-t-20">
            <div class="col-sm-6">
                <label for="phone">Phone</label>
                <input type="tel" class="form-control" name="phone" id="phone" pattern="^[0-9\s]{3,}$" required>
            </div>
            <div class="col-sm-6 m-t-12">
                <label for="city">City</label>
                <input type="text" class="form-control" name="city" id="city" required>
            </div>
        </div>
        <div class="row m0 p-t-20">
            <label for="message">Product Name</label>
            <input type="text" name="Productname" id="product-name" class="form-control" required></input>
        </div>

        <button class="btn btn-primary btn-lg filled p-t-20" type="submit">send</button>
        <div style="display:none" class="f-loading">
            <img src="loading.gif" alt="">
        </div>
        <div style="display:none" class="thankyou_message">
            <!-- You can customize the thankyou message by editing the code below -->
            <h2><em>Thanks</em> for coosing us! We will get back to you soon!
            </h2>
        </div>
    </form>
</div>

</body>

</html>