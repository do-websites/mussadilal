
<?php 
require_once("header.php"); 
function inputData($msdata){

    $msdata-> title="Musadilal | Service";
    $msdata-> description="Service description";
    $msdata-> keywords="Service keywords";

}
?>
<script>
    var element = document.getElementById("service_act");
  element.classList.add("active");

</script>

<section id="breadcrumbRow" class="row">
    <h2>Services</h2>
    <div class="row pageTitle m0">
        <div class="container">
            <h4 class="fleft">Services</h4>
            <ul class="breadcrumb fright">
                <li><a href="index.php">home</a></li>
                <li class="active">services</li>
            </ul>
        </div>
    </div>
</section>

<section id="headings" class="row shortcodesRow">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="row m0 mb30">
                    <h1 class="heading">Service 1</h1>
                    <p>Aenean eu leo quam. Pellentesque ornadsgxare semetrt mo lacinia quam venenatis vestibulum. Nulla vitae elit liberero, avelitu pharetra Etiamums malesuada. magna mollis euismod.</p>
                   <br>
                    <p class="dropcap dropcaFill">FNeque porro quisquam est, qui dolorem ipsum quia dolor sit amete conret sectetur, adipisci velit, sed quia non numquam eius modi tempora incistae dunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis
                    nostrum exercitationem ullam corporis suscipit laboriosam Neque porro quisquam est, qui dolorem ipsum quia dolor sit amete conret sectet man adipisci velit, sed quia non porro numquam.</p>
                </div>
                
            </div>
            <div class="col-sm-6">
                <div class="row m0 mb30">
                    <h2 class="heading">Service 2</h2>
                    
                    <br><p>Aenean eu leo quam. Pellentesque ornadsgxare semetrt mo lacinia quam venenatis vestibulum. Nulla vitae elit liberero, avelitu pharetra Etiamums malesuada. magna mollis euismod.</p>
                    <p class="dropcap dropcaFill">FNeque porro quisquam est, qui dolorem ipsum quia dolor sit amete conret sectetur, adipisci velit, sed quia non numquam eius modi tempora incistae dunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis
                    nostrum exercitationem ullam corporis suscipit laboriosam Neque porro quisquam est, qui dolorem ipsum quia dolor sit amete conret sectet man adipisci velit, sed quia non porro numquam.</p>
                </div>
                
            </div>
        </div>
    </div>
</section>



 <section id="serviceStyle2" class="row shortcodesRow">
        <div class="container">
            <h4 class="shortcodeHeading">Services Lorem</h4>
            <div class="row">
                <div class="col-sm-4 service2">
                    <div class="media">
                        <div class="media-left">
                            <a href="#"><img src="images/icons/cardm.png" alt="" class="media-object"></a>
                        </div>
                        <div class="media-body">
                            <h4 class="heading">Lowest Price</h4>
                            <p>We provide the best Quality of products to you.We are always here to help our lovely customers.</p>
                            <a href="#" class="readMore">Find out more  <i class="fas fa-angle-right"></i></a>
                        </div>
                    </div>
                </div> <!--service style 2-1- -->
                <div class="col-sm-4 service2">
                    <div class="media">
                        <div class="media-left">
                            <a href="#"><img src="images/icons/cardm.png" alt="" class="media-object"></a>
                        </div>
                        <div class="media-body">
                            <h4 class="heading">fast free delivery</h4>
                            <p>We provide the best Quality of products to you.We are always here to help our lovely customers.</p>
                            <a href="#" class="readMore">Find out more  <i class="fas fa-angle-right"></i></a>
                        </div>
                    </div>
                </div> <!--service style 2-2 -->
                <div class="col-sm-4 service2">
                    <div class="media">
                        <div class="media-left">
                            <a href="#"><img src="images/icons/cardm.png" alt="" class="media-object"></a>
                        </div>
                        <div class="media-body">
                            <h4 class="heading">24-7 help desk</h4>
                            <p>We provide the best Quality of products to you.We are always here to help our lovely customers.</p>
                            <a href="#" class="readMore">Find out more  <i class="fas fa-angle-right"></i></a>
                        </div>
                    </div>
                </div> <!--service style 2-3 -->
            </div>
        </div>
</section>


<section id="pricing" class="row shortcodesRow">
    <div class="container">
        <h4 class="shortcodeHeading">pricing style</h4>
        <div class="row">
            <div class="col-sm-4">
                <div class="pricing row m0">
                    <div class="row m0 pricingTitle">basic</div>
                    <div class="row features m0">
                        <div class="row m0 pricePerMonth"><i class="fas fa-usd"></i><span class="amount">175</span>per month</div>
                        <ul class="list-group">
                            <li class="list-group-item">3 Chairs</li>
                            <li class="list-group-item">Delivery in 5 Days</li>
                            <li class="list-group-item">1 Year Guarantee</li>
                            <li class="list-group-item">$50 For Shipping</li>
                        </ul>
                    </div>
                    <div class="row m0 joinNow"><a href="#">join now</a></div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="pricing row m0 active">
                    <div class="row m0 pricingTitle">premium</div>
                    <div class="row features m0">
                        <div class="row m0 pricePerMonth"><i class="fas fa-usd"></i><span class="amount">375</span>per month</div>
                        <ul class="list-group">
                            <li class="list-group-item">5 Chairs</li>
                            <li class="list-group-item">Delivery in 5 Days</li>
                            <li class="list-group-item">1 Year Guarantee</li>
                            <li class="list-group-item">Free Shipping</li>
                        </ul>
                    </div>
                    <div class="row m0 joinNow"><a href="#">join now</a></div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="pricing row m0">
                    <div class="row m0 pricingTitle">platinum</div>
                    <div class="row features m0">
                        <div class="row m0 pricePerMonth"><i class="fas fa-usd"></i><span class="amount">500</span>per month</div>
                        <ul class="list-group">
                            <li class="list-group-item">10 Chairs</li>
                            <li class="list-group-item">Delivery in 25 Days</li>
                            <li class="list-group-item">3 Year Guarantee</li>
                            <li class="list-group-item">Free Shipping</li>
                        </ul>
                    </div>
                    <div class="row m0 joinNow"><a href="#">join now</a></div>
                </div>
            </div>
        </div>
    </div>
</section>



<section id="brands" class="row shortcodesRow">
    <div class="container">
        <h4 class="shortcodeHeading row">brands style</h4>
        <div class="row brands">
            <ul class="nav navbar-nav">
                <li>
                    <a href="#"><img src="images/brands/1.png" alt=""></a>
                </li>
                <li>
                    <a href="#"><img src="images/brands/2.png" alt=""></a>
                </li>
                <li>
                    <a href="#"><img src="images/brands/3.png" alt=""></a>
                </li>
                <li>
                    <a href="#"><img src="images/brands/4.png" alt=""></a>
                </li>
                <li>
                    <a href="#"><img src="images/brands/5.png" alt=""></a>
                </li>
                <li>
                    <a href="#"><img src="images/brands/2.png" alt=""></a>
                </li>
            </ul>
        </div>
    </div>
</section>

<section id="contactBanner" class="row shortcodesRow">
    <div class="container">
        <div class="row">
            <div class="col-sm-9">
                <h3>Bring ideas in your life</h3>
                <h5>If you have any ideas! We have a special team to make your design</h5>
            </div>
            <div class="col-sm-3">
                <a href="contact.html" class="btn">contact us</a>
            </div>
        </div>
    </div>
</section>

<?php require_once("footer.php"); ?>