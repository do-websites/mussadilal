<?php
require_once("header.php");
function inputData($msdata)
{
    $msdata-> title="Musadilal | Product";
    $msdata-> description="product description";
    $msdata-> keywords="product keywords";
}
?>
<script>
var element = document.getElementById("drop_act"); // only for dropdown menu items
element.classList.add("active");

/* var element = document.getElementById("diamond_act");
element.classList.add("active"); */
</script>

<section id="breadcrumbRow" class="row">
    <h2 style="background: url(images/gold-hero.jpg) !important;">Gold</h2>
    <div class="row pageTitle m0">
        <div class="container">
            <h4 class="fleft">Gold</h4>
            <ul class="breadcrumb fright">
                <li><a href="index.php">home</a></li>
                <li><a href="index.php">Products</a></li>
                <li class="active">Gold</li>
            </ul>
        </div>
    </div>
</section>

<section id="Musaddilal" class="row contentRowPad">
    <div class="container">
        <div class="row sectionTitle">
            <h2>Gold Jewellery </h2>
            <h5>Our unique, custom-made gold jewellery with antique finish are perfect for festivals, parties, and
                weddings.</h5>
        </div>
        <div class="row">
            <div class="col-sm-4 product">
                <div class="productInner row m0">
                    <div class="row m0 imgHov">
                        <img src="products/gold/1NTIQUE-FINISH-NAKSHI-AND-KUNDAN-MANGO-PIECES-HAARAM.jpg" alt="gold">
                        <div class="row m0 hovArea">
                            <div class="row m0 proType">Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry. Lorem Ipsum has been the industry's standard dummy text ever since</div>
                        </div>
                    </div>
                    <div class="row m0 proName"><a href="#">Antique Finish Nakshi And Kundan Mango Pieces Haaram</a></div>
                    <div class="row m0 proBuyBtn">
                        <a class="addToCart btn" href="#pqen" rel="modal:open">Product Enquiry</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 product">
                <div class="productInner row m0">
                    <div class="row m0 imgHov">
                    <img src="products/gold/ANTIQUE-FINISH-NAKSHI-AND-KUNDAN-MANGO-PIECES-HAARAM.jpg" alt="gold">
                        <div class="row m0 hovArea">
                            <div class="row m0 proType">Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry. Lorem Ipsum has been the industry's standard dummy text ever since</div>
                        </div>
                    </div>
                    <div class="row m0 proName"><a href="#">Antique Finish Nakshi And Kundan Mango Pieces Haaram</a></div>
                    <div class="row m0 proBuyBtn">
                        <a class="addToCart btn" href="#pqen" rel="modal:open">Product Enquiry</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 product">
                <div class="productInner row m0">
                    <div class="row m0 imgHov">
                    <img src="products/gold/GOLD-WORK-ANTIQUE-HAARAM-WITH-RED-CABOCHONS-AND-PEARLS.jpg" alt="gold">
                        <div class="row m0 hovArea">
                            <div class="row m0 proType">Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry. Lorem Ipsum has been the industry's standard dummy text ever since</div>
                        </div>
                    </div>
                    <div class="row m0 proName"><a href="#">Gold Work Antique Haaram With Red Cabochons And Pearls</a></div>
                    <div class="row m0 proBuyBtn">
                        <a class="addToCart btn" href="#pqen" rel="modal:open">Product Enquiry</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 product">
                <div class="productInner row m0">
                    <div class="row m0 imgHov">
                    <img src="products/gold/MANGO-MALA-GOLD.jpg" alt="gold">
                        <div class="row m0 hovArea">
                            <div class="row m0 proType">Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry. Lorem Ipsum has been the industry's standard dummy text ever since</div>
                        </div>
                    </div>
                    <div class="row m0 proName"><a href="#">Mango Mala Gold</a></div>
                    <div class="row m0 proBuyBtn">
                        <a class="addToCart btn" href="#pqen" rel="modal:open">Product Enquiry</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 product">
                <div class="productInner row m0">
                    <div class="row m0 imgHov">
                    <img src="products/gold/ANTIQUE-GOLD-NAKSHI-LONG-HAARAM.jpg" alt="gold">
                        <div class="row m0 hovArea">

                            <div class="row m0 proType">Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry. Lorem Ipsum has been the industry's standard dummy text ever since</div>
                        </div>
                    </div>
                    <div class="row m0 proName"><a href="#">Antique Gold Nakshi Long Haaram</a></div>
                    <div class="row m0 proBuyBtn">
                        <a class="addToCart btn" href="#pqen" rel="modal:open">Product Enquiry</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 product">
                <div class="productInner row m0">
                    <div class="row m0 imgHov">
                    <img src="products/gold/ANTIQUE-WORK-GOLD-LONG-HAARAM.jpg" alt="gold">
                        <div class="row m0 hovArea">
                            <div class="row m0 proType">Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry. Lorem Ipsum has been the industry's standard dummy text ever since</div>
                        </div>
                    </div>
                    <div class="row m0 proName"><a href="#">Antique Work Gold Long Haaram</a></div>
                    <div class="row m0 proBuyBtn">
                        <a class="addToCart btn" href="#pqen" rel="modal:open">Product Enquiry</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 product">
                <div class="productInner row m0">
                    <div class="row m0 imgHov">
                    <img src="products/gold/NAKSHI-GOLD-HAARAM-WITH-LAKSHMI-LOCKET.jpg" alt="gold">
                        <div class="row m0 hovArea">
                            <div class="row m0 proType">Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry. Lorem Ipsum has been the industry's standard dummy text ever since</div>
                        </div>
                    </div>
                    <div class="row m0 proName"><a href="#">Nakshi Gold Haaram With Lakshmi Locket</a></div>
                    <div class="row m0 proBuyBtn">
                        <a class="addToCart btn" href="#pqen" rel="modal:open">Product Enquiry</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 product">
                <div class="productInner row m0">
                    <div class="row m0 imgHov">
                    <img src="products/gold/SOUTH-SEA-MALA-WITH-TOURMALINE-BEADS.jpg" alt="gold">
                        <div class="row m0 hovArea">
                            <div class="row m0 proType">Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry. Lorem Ipsum has been the industry's standard dummy text ever since</div>
                        </div>
                    </div>
                    <div class="row m0 proName"><a href="#">South Sea Mala With Tourmaline Beads</a></div>
                    <div class="row m0 proBuyBtn">
                        <a class="addToCart btn" href="#pqen" rel="modal:open">Product Enquiry</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 product">
                <div class="productInner row m0">
                    <div class="row m0 imgHov">
                    <img src="products/gold/ANTIQUE-NAKSHI-HAARAM-WITH-UNCUT-DIAMOND-AND-PRECIOUS-STONES.jpg" alt="gold">
                        <div class="row m0 hovArea">
                            <div class="row m0 proType">Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry. Lorem Ipsum has been the industry's standard dummy text ever since</div>
                        </div>
                    </div>
                    <div class="row m0 proName"><a href="#">Antique Nakshi Haaram With Uncut Diamond And Precious Stones</a></div>
                    <div class="row m0 proBuyBtn">
                        <a class="addToCart btn" href="#pqen" rel="modal:open">Product Enquiry</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 product">
                <div class="productInner row m0">
                    <div class="row m0 imgHov">
                    <img src="products/gold/ANTIQUE-GOLD-HAARAM-WITH-KUNDAN-AND-NAKSHI-WORK-WITH-RED-CABOCHONS-AND-PEARLS.jpg" alt="gold">
                        <div class="row m0 hovArea">
                            <div class="row m0 proType">Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry. Lorem Ipsum has been the industry's standard dummy text ever since</div>
                        </div>
                    </div>
                    <div class="row m0 proName"><a href="#">Antique Gold Haaram With Kundan And Nakshi Work With Red Cabochons And Pearls</a></div>
                    <div class="row m0 proBuyBtn">
                        <a class="addToCart btn" href="#pqen" rel="modal:open">Product Enquiry</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 product">
                <div class="productInner row m0">
                    <div class="row m0 imgHov">
                    <img src="products/gold/TRADITIONAL-ANTIQUE-NAKSHI-HAARAM-WITH-SOUTH-INIDAN-KUNDAN-WORK.jpg" alt="gold">
                        <div class="row m0 hovArea">
                            <div class="row m0 proType">Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry. Lorem Ipsum has been the industry's standard dummy text ever since</div>
                        </div>
                    </div>
                    <div class="row m0 proName"><a href="#">Traditional Antique  Nakshi Haaram With South Inidan Kundan Work</a></div>
                    <div class="row m0 proBuyBtn">
                        <a class="addToCart btn" href="#pqen" rel="modal:open">Product Enquiry</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 product">
                <div class="productInner row m0">
                    <div class="row m0 imgHov">
                    <img src="products/gold/NAKSHI-WORK-HAARAM-WITH-KUNDAN-WORK-WITH-UNCUT-DIAMONDS-AND-CABOCHON-RUBIES.jpg" alt="gold">
                        <div class="row m0 hovArea">
                            <div class="row m0 proType">Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry. Lorem Ipsum has been the industry's standard dummy text ever since</div>
                        </div>
                    </div>
                    <div class="row m0 proName"><a href="#">Nakshi Work Haaram With Kundan Work With Uncut Diamonds And Cabochon Rubies</a></div>
                    <div class="row m0 proBuyBtn">
                        <a class="addToCart btn" href="#pqen" rel="modal:open">Product Enquiry</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php require_once("footer.php"); ?>