<?php 
require_once("header.php"); 
function inputData($msdata){

    $msdata-> title="Musadilal";
    $msdata-> description="Hello ms";
    $msdata-> keywords="need to add keywords";

}
?>
<script>
var element = document.getElementById("home_act");
element.classList.add("active");
</script>

<section id="slider" class="row">
    <!--  <div class="row sliderCont flexslider m0">
        <ul class="slides nav">
         
            <li>
                <img src="images/slider/a.jpg" alt="">
                <div class="text_lines row m0">
                    <div class="container p0 text-left">
                        <h3>exquisite Designer Rings</h3>
                        <h2>new collections</h2>
                        <h4><a class="theme_btn with_i" href="#"><i class="fas fa-plus-circle"></i>View now</a></h4>
                    </div>
                </div>
               
            </li>
            <li>
                <img src="images/slider/d.jpg" alt="">
                <div class="text_lines row m0">
                    <div class="container p0">
                        <h3>traditional Designer Jwellery</h3>
                        <h2>new collections</h2>
                        <h4><a class="theme_btn with_i" href="#"><i class="fas fa-plus-circle"></i>View now</a></h4>
                    </div>
                </div>
               
            </li>
            <li>
                <img src="images/slider/c.jpg" alt="">
                <div class="text_lines row m0">
                    <div class="container p0 text-left">
                        <h3>Find your perfect diamond</h3>
                        <h2>By Musaddilal</h2>
                        <h4><a class="theme_btn with_i" href="#"><i class="fas fa-plus-circle"></i>View now</a></h4>
                    </div>
                </div>
              
            </li>
        </ul>
    </div> -->
    <div id="carousel-desktop-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carousel-desktop-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-desktop-generic" data-slide-to="1"></li>
            <li data-target="#carousel-desktop-generic" data-slide-to="2"></li>
            <li data-target="#carousel-desktop-generic" data-slide-to="3"></li>

        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="images/msbanner1.jpg">
            </div>
            <div class="item">
                <img src="images/msbanner2.jpg">
            </div>
            <div class="item">
                <img src="images/Banner2u.jpg">
            </div>
            <div class="item">
                <img src="images/banner-4.jpg">
            </div>
        </div>

        <!-- Controls -->
        <!--  <a class="left carousel-control" href="#carousel-desktop-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-desktop-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a> -->
    </div>

    <div id="carousel-mobile-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carousel-mobile-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-mobile-generic" data-slide-to="1"></li>
            <li data-target="#carousel-mobile-generic" data-slide-to="2"></li>
            <li data-target="#carousel-mobile-generic" data-slide-to="3"></li>

        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="images/Banner2um.jpg">
            </div>
            <div class="item">
                <img src="images/msmb2.jpg">
            </div>
            <div class="item">
                <img src="images/msmb3.jpg">
            </div>
            <div class="item">
                <img src="images/banner-4.jpg-mobile.jpg">
            </div>
        </div>

        <!-- Controls -->
        <!--  <a class="left carousel-control" href="#carousel-mobile-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-mobile-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a> -->
    </div>



</section>

<section id="ring_sec" class="ring_sec">
    <div id="trigger" class="container ">
        <div class="row">
            <div class="col-md-6  diamond_j_cont">
                <div class="diamond_j">
                </div>
                <div class="diamond_b">
                    <img alt="" class="img-responsive" src="images/ring.png">
                </div>
            </div>
            <div class="col-md-6  ring_cont">
                <h2>About Musaddilal</h2>
                <p>MUSADDILAL &amp; SONS is known for its high-quality product, exclusivity &amp; exemplary service.
                    With over a century old experience in catering to the needs of clients from all across the globe.
                </p>
                <p>
                    Our journey began at the turn of the 20th century, founders of the group were silver &amp; gold
                    merchants, occasionally dealing in precious gemstones &amp; pearls. The group is named MUSADDILAL
                    after the second generation of the early founder.
                </p>
                <a class="com_btn" href="about.php">Know More</a>
            </div>
        </div>
    </div>
</section>


<!-- <section id="shopRings">
    <div class="sectionTitle">
        <h3>New Designs</h3>
        <h5>know more about our latest collection</h5>
    </div>
    <div class="d-carousel-cener owl-carousel">
        <div class="dc-inner">
            <a href="#">
                <img alt="ring" src="images/carousel/1.png">
                <div class="dc-containt">
                    <h2>Cushion</h2>
                    <p>Bar Set Anniversary Ring</p>
                </div>
            </a>
        </div>
        <div class="dc-inner">
            <a href="#">
                <img alt="ring" src="images/carousel/2.png">
                <div class="dc-containt">
                    <h2>Round</h2>
                    <p>Bar Set Anniversary Ring</p>
                </div>
            </a>
        </div>
        <div class="dc-inner">
            <a href="#">
                <img alt="ring" src="images/carousel/3.png">
                <div class="dc-containt">
                    <h2>Pear</h2>
                    <p>Bar Set Anniversary Ring</p>
                </div>
            </a>
        </div>
        <div class="dc-inner">
            <a href="#">
                <img alt="ring" src="images/carousel/4.png">
                <div class="dc-containt">
                    <h2>Radiant</h2>
                    <p>Bar Set Anniversary Ring</p>
                </div>
            </a>
        </div>
        <div class="dc-inner">
            <a href="#">
                <img alt="ring" src="images/carousel/5.png">
                <div class="dc-containt">
                    <h2>Ovel</h2>
                    <p>Bar Set Anniversary Ring</p>
                </div>
            </a>
        </div>
    </div>
</section> -->

<!-- 
<section id="shopFeatures_new">
    <div class=" shopFeatures_new container">
        <ul>
            <li class="sf_first">
                <img alt="" class="img-responsive" src="images/feature/a.jpg">
                <div class="sf_box">
                    <div class="sf_box_inner">
                        <h2>Want to Know More!</h2>
                        <h3>We are here to help you</h3>
                        <a class="com_btn" href="#">Contact Us</a>
                    </div>
                </div>
            </li>
             <li>
                <a href="product.html">
                    <img alt="" class="img-responsive" src="images/feature/2.jpg">
                    <div class="sf_box">
                        <div class="sf_box_inner">
                            <h3>shop & save</h3>
                            <p>On all our store items</p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="catalog.html">
                    <img alt="" class="img-responsive" src="images/feature/3.jpg">
                    <div class="sf_box">
                        <div class="sf_box_inner">
                            <h3>Product catalog</h3>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="product.html">
                    <img alt="" class="img-responsive" src="images/feature/4.jpg">
                    <div class="sf_box">
                        <div class="sf_box_inner">
                            <h3>product list</h3>
                            <p>Lorem ipsum dolor sit amet</p>
                        </div>
                    </div>
                </a>
            </li> 
        </ul>
    </div>
</section>
-->

<!--Feature Products 4 Collumn-->
<section id="featureCategory" class="row contentRowPad">
    <div class="container">
        <div class="row m0 sectionTitle">
            <h3>our product categories</h3>
            <h5>From the house of M&S Jewels, Gems and More </h5>
        </div>
        <div class="owl-carousel featureCats row m0">
            <div class="item">
                <a href="product-diamond.php">
                    <div class="row m0 imgHov">
                        <img src="images/saiarlen/categories/diamond.jpg" alt="diamond">
                        <div class="row m0 hovArea"></div>
                    </div>
                </a>

            </div>
            <div class="item">
                <a href="product-gold.php">
                    <div class="row m0 imgHov">
                        <img src="images/saiarlen/categories/gold.jpg" alt="gold">
                        <div class="row m0 hovArea"></div>

                    </div>
                </a>
            </div>
            <div class="item">
                <a href="product-kundan.php">
                    <div class="row m0 imgHov">
                        <img src="images/saiarlen/categories/kundan.jpg" alt="kundan">
                        <div class="row m0 hovArea"></div>

                    </div>
                </a>
            </div>
            <div class="item">
                <a href="product-polki.php">
                    <div class="row m0 imgHov">
                        <img src="images/saiarlen/categories/pearl.jpg" alt="pearl">
                        <div class="row m0 hovArea"></div>
                        <!-- <div class="row m0 hovArea">
                        <i class="fas fa-heart-o"></i><br>
                         <h4>55 items</h4>
                        <a href="#">Open Now</a>
                    </div> -->
                    </div>
                </a>
                <!-- <div class="cat_h">
                    <a href="#">
                        <h4>Certification 5</h4>
                        <span>See the certificate</span>
                    </a>
                </div> -->

            </div>

        </div>
    </div>
</section>
<!--Feature Categories-->
<!-- <section id="Musaddilal" class="row contentRowPad">
    <div class="container">
        <div class="row sectionTitle">
            <h3>this is Musaddilal</h3>
            <h5>shop over our best brands</h5>
        </div>
        <div class="row">
            <div class="col-sm-4 product">
                <div class="productInner row m0">
                    <div class="row m0 imgHov">
                        <img src="images/product/5.png" alt="">
                        <div class="row m0 hovArea">
                            <div class="row m0 icons">
                                <ul class="list-inline">
                                    <li><a href="#"><i class="fas fa-heart"></i></a></li>
                                    <li><a href="#"><i class="fas fa-shopping-cart-alt"></i></a></li>
                                    <li><a href="#"><i class="fas fa-expand"></i></a></li>
                                </ul>
                            </div>
                            <div class="row m0 proType"><a href="#">Baccarat</a></div>
                            <div class="row m0 proRating">
                                <i class="fas fa-star-o"></i>
                                <i class="fas fa-star-o"></i>
                                <i class="fas fa-star-o"></i>
                                <i class="fas fa-star-o"></i>
                                <i class="fas fa-star-o"></i>
                            </div>
                            <div class="row m0 proPrice"><i class="fas fa-usd"></i>455.00</div>
                        </div>
                    </div>
                    <div class="row m0 proName"><a href="#">Maximus quam posuerea</a></div>
                    <div class="row m0 proBuyBtn">
                        <button class="addToCart btn">add to cart</button>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 product">
                <div class="productInner row m0">
                    <div class="row m0 imgHov">
                        <img src="images/product/6.png" alt="">
                        <div class="row m0 hovArea">
                            <div class="row m0 icons">
                                <ul class="list-inline">
                                    <li><a href="#"><i class="fas fa-heart"></i></a></li>
                                    <li><a href="#"><i class="fas fa-shopping-cart-alt"></i></a></li>
                                    <li><a href="#"><i class="fas fa-expand"></i></a></li>
                                </ul>
                            </div>
                            <div class="row m0 proType"><a href="#">Baccarat</a></div>
                            <div class="row m0 proRating">
                                <i class="fas fa-star-o"></i>
                                <i class="fas fa-star-o"></i>
                                <i class="fas fa-star-o"></i>
                                <i class="fas fa-star-o"></i>
                                <i class="fas fa-star-o"></i>
                            </div>
                            <div class="row m0 proPrice"><i class="fas fa-usd"></i>830.00</div>
                        </div>
                    </div>
                    <div class="row m0 proName"><a href="#">Gravida est</a></div>
                    <div class="row m0 proBuyBtn">
                        <a class="addToCart btn" href="#">add to cart</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 product">
                <div class="productInner row m0">
                    <div class="row m0 imgHov">
                        <img src="images/product/7.png" alt="">
                        <div class="row m0 hovArea">
                            <div class="row m0 icons">
                                <ul class="list-inline">
                                    <li><a href="#"><i class="fas fa-heart"></i></a></li>
                                    <li><a href="#"><i class="fas fa-shopping-cart-alt"></i></a></li>
                                    <li><a href="#"><i class="fas fa-expand"></i></a></li>
                                </ul>
                            </div>
                            <div class="row m0 proType"><a href="#">Baccarat</a></div>
                            <div class="row m0 proRating">
                                <i class="fas fa-star-o"></i>
                                <i class="fas fa-star-o"></i>
                                <i class="fas fa-star-o"></i>
                                <i class="fas fa-star-o"></i>
                                <i class="fas fa-star-o"></i>
                            </div>
                            <div class="row m0 proPrice"><i class="fas fa-usd"></i>525.00</div>
                        </div>
                    </div>
                    <div class="row m0 proName"><a href="#">Donec aliquam</a></div>
                    <div class="row m0 proBuyBtn">
                        <button class="addToCart btn">add to cart</button>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 product">
                <div class="productInner row m0">
                    <div class="row m0 imgHov">
                        <img src="images/product/8.png" alt="">
                        <div class="row m0 hovArea">
                            <div class="row m0 icons">
                                <ul class="list-inline">
                                    <li><a href="#"><i class="fas fa-heart"></i></a></li>
                                    <li><a href="#"><i class="fas fa-shopping-cart-alt"></i></a></li>
                                    <li><a href="#"><i class="fas fa-expand"></i></a></li>
                                </ul>
                            </div>
                            <div class="row m0 proType"><a href="#">Baccarat</a></div>
                            <div class="row m0 proRating">
                                <i class="fas fa-star-o"></i>
                                <i class="fas fa-star-o"></i>
                                <i class="fas fa-star-o"></i>
                                <i class="fas fa-star-o"></i>
                                <i class="fas fa-star-o"></i>
                            </div>
                            <div class="row m0 proPrice"><i class="fas fa-usd"></i>1025.00</div>
                        </div>
                    </div>
                    <div class="row m0 proName"><a href="#">Curabitur cursus dignis</a></div>
                    <div class="row m0 proBuyBtn">
                        <button class="addToCart btn">add to cart</button>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 product">
                <div class="productInner row m0">
                    <div class="row m0 imgHov">
                        <img src="images/product/9.png" alt="">
                        <div class="row m0 hovArea">
                            <div class="row m0 icons">
                                <ul class="list-inline">
                                    <li><a href="#"><i class="fas fa-heart"></i></a></li>
                                    <li><a href="#"><i class="fas fa-shopping-cart-alt"></i></a></li>
                                    <li><a href="#"><i class="fas fa-expand"></i></a></li>
                                </ul>
                            </div>
                            <div class="row m0 proType"><a href="#">Baccarat</a></div>
                            <div class="row m0 proRating">
                                <i class="fas fa-star-o"></i>
                                <i class="fas fa-star-o"></i>
                                <i class="fas fa-star-o"></i>
                                <i class="fas fa-star-o"></i>
                                <i class="fas fa-star-o"></i>
                            </div>
                            <div class="row m0 proPrice"><i class="fas fa-usd"></i>725.00</div>
                        </div>
                    </div>
                    <div class="row m0 proName"><a href="#">Curabitur cursus dignis</a></div>
                    <div class="row m0 proBuyBtn">
                        <button class="addToCart btn">add to cart</button>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 product">
                <div class="productInner row m0">
                    <div class="row m0 imgHov">
                        <img src="images/product/10.png" alt="">
                        <div class="row m0 hovArea">
                            <div class="row m0 icons">
                                <ul class="list-inline">
                                    <li><a href="#"><i class="fas fa-heart"></i></a></li>
                                    <li><a href="#"><i class="fas fa-shopping-cart-alt"></i></a></li>
                                    <li><a href="#"><i class="fas fa-expand"></i></a></li>
                                </ul>
                            </div>
                            <div class="row m0 proType"><a href="#">Baccarat</a></div>
                            <div class="row m0 proRating">
                                <i class="fas fa-star-o"></i>
                                <i class="fas fa-star-o"></i>
                                <i class="fas fa-star-o"></i>
                                <i class="fas fa-star-o"></i>
                                <i class="fas fa-star-o"></i>
                            </div>
                            <div class="row m0 proPrice"><i class="fas fa-usd"></i>825.00</div>
                        </div>
                    </div>
                    <div class="row m0 proName"><a href="#">Curabitur cursus dignis</a></div>
                    <div class="row m0 proBuyBtn">
                        <button class="addToCart btn">add to cart</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> -->
<section id="parallelbnrs">
    <div class="deskcon">
        <img class="img-responsive" src="images/banner-below.jpg" data-appointlet-organization="musaddilal" data-appointlet-bookable="145430" alt="banner">
        <!-- <img class="img-responsive" src="images/saiarlen/bnrcats/gold.jpg" alt="diamond"> -->
      <!--   <img class="img-responsive" src="images/saiarlen/bnrcats/pearl.jpg" alt="diamond"> -->
    </div>
    <div class="mobcon">
        <img  class="img-responsive" src="images/banner-below-m.jpg" data-appointlet-organization="musaddilal" data-appointlet-bookable="145430" alt="banner">
        <!-- <img class="img-responsive" src="images/saiarlen/bnrcats/gold-m.jpg" alt="diamond"> -->
      <!--   <img class="img-responsive" src="images/saiarlen/bnrcats/pearl-m.jpg" alt="diamond"> -->
    </div>
   
</section>
<!--Feature Products 4 Collumn-->
<section id="testimonialTabs" class="row contentRowPad">
    <div class="container">
        <div class="row sectionTitle">
            <h3>some words from our customers</h3>
            <h5>A heartfelt note from our Customers Testimonials </h5>
        </div>
        <div class="row">
            <div class="tab-content testiTabContent">
                <div role="tabpanel" class="tab-pane active" id="testi1">
                    <p><span class="t_q_start">“</span>I do love jewellery designs that are unique, delicate and striking - all of which M&S's pieces are. Not to mention the quality - just gorgeous.<span class="t_q_end">”</span></p>
                    <h5 class="customerName">Srijita Nandi</h5>
                </div>
                <div role="tabpanel" class="tab-pane" id="testi2">
                    <p><span class="t_q_start">“</span>Thank you, M&S - your work is just beautiful. I've had so many compliments when wearing my pendant. I just love it!
                        <span class="t_q_end">”</span>
                    </p>
                    <h5 class="customerName">Ayushi Thakur</h5>
                </div>
                <div role="tabpanel" class="tab-pane" id="testi3">
                    <p><span class="t_q_start">“</span> MThe pieces are exactly as they appear on the website, contemporary with a nod to classic design and the quality is of a high standard ... Love it! <span class="t_q_end">”</span></p>
                    <h5 class="customerName">Amrita Reddy</h5>
                </div>
                <div role="tabpanel" class="tab-pane" id="testi4">
                    <p><span class="t_q_start">“</span> I feel lucky to have found the delicate, unique and special necklaces and earrings that I have purchased from Musaddilal. I am always happy to recommend this fabulous site and shop<span class="t_q_end">”</span></p>
                    <h5 class="customerName">Trishna</h5>
                </div>
                
            </div>
            <ul class="nav nav-tabs" role="tablist" id="testiTab">
                <li role="presentation" class="active">
                    <a href="#testi1" aria-controls="testi1" role="tab" data-toggle="tab">
                        <img src="images/testimonial/tm1.jpg" alt="">
                    </a>
                    <div class="testi_rating">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star-half"></i>
                    </div>
                </li>
                <li role="presentation">
                    <a href="#testi2" aria-controls="testi2" role="tab" data-toggle="tab">
                        <img src="images/testimonial/tm2.jpg" alt="">
                    </a>
                    <div class="testi_rating">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star-o"></i>
                        <i class="fas fa-star-o"></i>
                    </div>
                </li>
                <li role="presentation">
                    <a href="#testi3" aria-controls="testi3" role="tab" data-toggle="tab">
                        <img src="images/testimonial/tm3.jpg" alt="">
                    </a>
                    <div class="testi_rating">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                    </div>
                </li>
                <li role="presentation">
                    <a href="#testi4" aria-controls="testi4" role="tab" data-toggle="tab">
                        <img src="images/testimonial/tm4.jpg" alt="">
                    </a>
                    <div class="testi_rating">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star-o"></i>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</section>
<!--Testimonial Tabs-->
<!-- <section id="brands" class="row contentRowPad">
    <div class="container">
        <div class="row sectionTitle">
            <h3>our brands</h3>
            <h5>choose best with our favorite brands</h5>
        </div>
        <div class="row brands">
            <ul class="nav navbar-nav">
                <li>
                    <a href="#"><img src="images/brands/1.png" alt=""></a>
                </li>
                <li>
                    <a href="#"><img src="images/brands/2.png" alt=""></a>
                </li>
                <li>
                    <a href="#"><img src="images/brands/3.png" alt=""></a>
                </li>
                <li>
                    <a href="#"><img src="images/brands/4.png" alt=""></a>
                </li>
                <li>
                    <a href="#"><img src="images/brands/5.png" alt=""></a>
                </li>
                <li>
                    <a href="#"><img src="images/brands/2.png" alt=""></a>
                </li>
            </ul>
        </div>
    </div>
</section> -->
<!-- <section id="homeBlog">
    <div class="container blog_j">
        <div class="row sectionTitle">
            <h3>Blog Updates</h3>
            <h5>we satisfied more than 700 customers</h5>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="blog_inner single">
                    <div class="blog_j_img">
                        <img alt="" class="img-responsive" src="images/blog/blog6.png">
                        <div class="btn_readmore">
                            <a href="blog.html">Read more</a>
                        </div>
                    </div>
                    <div class="blog_j_text">
                        <p>Discover our unriveled selection of must-have jewellery in timeless styles.
                        </p>
                        <span>December 2017
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="blog_inner">
                            <div class="blog_j_img">
                                <img alt="" class="img-responsive" src="images/blog/blog1.png">
                                <div class="btn_readmore">
                                    <a href="blog.html">Read more</a>
                                </div>
                            </div>
                            <div class="blog_j_text">
                                <p>Discover our finely curated collection
                                </p>
                            </div>
                        </div>
                        <div class="blog_inner">
                            <div class="blog_j_img">
                                <img alt="" class="img-responsive" src="images/blog/blog5.png">
                                <div class="btn_readmore">
                                    <a href="blog.html">Read more</a>
                                </div>
                            </div>
                            <div class="blog_j_text">
                                <p>Discover our finely curated collection
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="blog_inner">
                            <div class="blog_j_img">
                                <img alt="" class="img-responsive" src="images/blog/blog7.png">
                                <div class="btn_readmore">
                                    <a href="blog.html">Read more</a>
                                </div>
                            </div>
                            <div class="blog_j_text">
                                <p>Discover our finely curated collection
                                </p>
                            </div>
                        </div>
                        <div class="blog_inner">
                            <div class="blog_j_img">
                                <img alt="" class="img-responsive" src="images/blog/blog3.png">
                                <div class="btn_readmore">
                                    <a href="blog.html">Read more</a>
                                </div>
                            </div>
                            <div class="blog_j_text">
                                <p>Discover our finely curated collection
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> -->
<?php require_once("footer.php"); ?>