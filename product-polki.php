<?php
require_once("header.php");
function inputData($msdata)
{
    $msdata-> title="Musadilal | Product";
    $msdata-> description="product description";
    $msdata-> keywords="product keywords";
}
?>
<script>
var element = document.getElementById("drop_act"); // only for dropdown menu items
element.classList.add("active");

/* var element = document.getElementById("diamond_act");
element.classList.add("active"); */
</script>

<section id="breadcrumbRow" class="row">
    <h2 style="background: url(images/pearl-hero.jpg) !important;">Polki</h2>
    <div class="row pageTitle m0">
        <div class="container">
            <h4 class="fleft">Polki</h4>
            <ul class="breadcrumb fright">
                <li><a href="index.php">home</a></li>
                <li><a href="index.php">Products</a></li>
                <li class="active">Polki</li>
            </ul>
        </div>
    </div>
</section>

<section id="Musaddilal" class="row contentRowPad">
    <div class="container">
        <div class="row sectionTitle">
            <h2>Polki Jewellery </h2>
            <h5>Our unique, custom-made pearls jewellery with antique finish are perfect for festivals, parties, and
                weddings.</h5>
        </div>
        <div class="row">
            <div class="col-sm-4 product">
                <div class="productInner row m0">
                    <div class="row m0 imgHov">
                        <img src="products/polki/1-TRADITIONAL-RAM-DURBAR-HAAR-WITH-UNCUT-DIAMONS-IN-PACCHI-WORK.jpg"
                            alt="pearl">
                        <div class="row m0 hovArea">
                            <div class="row m0 proType">Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry. Lorem Ipsum has been the industry's standard dummy text ever since</div>
                        </div>
                    </div>
                    <div class="row m0 proName"><a href="#">Traditional Ram Durbar Haar With Uncut Diamons In Pacchi
                            Work</a></div>
                    <div class="row m0 proBuyBtn">
                        <a class="addToCart btn" href="#pqen" rel="modal:open">Product Enquiry</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 product">
                <div class="productInner row m0">
                    <div class="row m0 imgHov">
                        <img src="products/polki/TRADITIONAL-RAM-DURBAR-HAAR-WITH-UNCUT-DIAMONS-IN-PACCHI-WORK.jpg"
                            alt="pearl">
                        <div class="row m0 hovArea">
                            <div class="row m0 proType">Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry. Lorem Ipsum has been the industry's standard dummy text ever since</div>
                        </div>
                    </div>
                    <div class="row m0 proName"><a href="#">Traditional Ram Durbar Haar With Uncut Diamons In Pacchi
                            Work</a></div>
                    <div class="row m0 proBuyBtn">
                        <a class="addToCart btn" href="#pqen" rel="modal:open">Product Enquiry</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 product">
                <div class="productInner row m0">
                    <div class="row m0 imgHov">
                        <img src="products/polki/UNCUT-DIAMOND-CHOKER-IN-PACCHI-WORK-WITH-PEARLS-AND-EMERALD-STONES.jpg"
                            alt="pearl">
                        <div class="row m0 hovArea">
                            <div class="row m0 proType">Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry. Lorem Ipsum has been the industry's standard dummy text ever since</div>
                        </div>
                    </div>
                    <div class="row m0 proName"><a href="#">Uncut Diamond Choker In Pacchi Work With Pearls And Emerald
                            Stones</a></div>
                    <div class="row m0 proBuyBtn">
                        <a class="addToCart btn" href="#pqen" rel="modal:open">Product Enquiry</a>
                    </div>
                </div>
            </div>
            <div class="row">
            <div class="col-sm-4 product">
                <div class="productInner row m0">
                    <div class="row m0 imgHov">
                        <img src="products/polki/PACCHI-AND-NAKSHI-WORK-NECKLACE-WITH-UNCUT-DIAMONDS-AND-RUBY-AND-EMERALD-CABOCHONS.jpg"
                            alt="pearl">
                        <div class="row m0 hovArea">
                            <div class="row m0 proType">Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry. Lorem Ipsum has been the industry's standard dummy text ever since</div>
                        </div>
                    </div>
                    <div class="row m0 proName"><a href="#">Pacchi And Nakshi Work Necklace With Uncut Diamonds And Ruby
                            And Emerald Cabochons</a></div>
                    <div class="row m0 proBuyBtn">
                        <a class="addToCart btn" href="#pqen" rel="modal:open">Product Enquiry</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 product">
                <div class="productInner row m0">
                    <div class="row m0 imgHov">
                        <img src="products/polki/NAKSHI-PACCHI-WORK-KANTI-NECKLACE-WITH LOCKET-IN-UNCUT-DIAMONDS-PEARLS-AND-PRECIOUS-STONES.jpg"
                            alt="pearl">
                        <div class="row m0 hovArea">

                            <div class="row m0 proType">Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry. Lorem Ipsum has been the industry's standard dummy text ever since</div>
                        </div>
                    </div>
                    <div class="row m0 proName"><a href="#">Nakshi Pacchi Work Kanti Necklace With Locket In Uncut
                            Diamonds , Pearls And Precious Stones </a></div>
                    <div class="row m0 proBuyBtn">
                        <a class="addToCart btn" href="#pqen" rel="modal:open">Product Enquiry</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 product">
                <div class="productInner row m0">
                    <div class="row m0 imgHov">
                        <img src="products/polki/PACCHI-HAARAM-WITH-JHUMKIS-IN-UNCUT-DIAMONDS -PEARLS-AND-COLOR-STONES.jpg"
                            alt="pearl">
                        <div class="row m0 hovArea">
                            <div class="row m0 proType">Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry. Lorem Ipsum has been the industry's standard dummy text ever since</div>
                        </div>
                    </div>
                    <div class="row m0 proName"><a href="#">Pacchi Haaram With Jhumkis In Uncut Diamonds, Pearls And
                            Color Stones</a></div>
                    <div class="row m0 proBuyBtn">
                        <a class="addToCart btn" href="#pqen" rel="modal:open">Product Enquiry</a>
                    </div>
                </div>
            </div>
            </div>
            
            <div class="row">
                <div class="col-sm-4 product">
                    <div class="productInner row m0">
                        <div class="row m0 imgHov">
                            <img src="products/polki/PACCHI-AND-NAKSHI-HARAM-WITH-UNCUT-DIAMONDS.jpg" alt="pearl">
                            <div class="row m0 hovArea">
                                <div class="row m0 proType">Lorem Ipsum is simply dummy text of the printing and
                                    typesetting
                                    industry. Lorem Ipsum has been the industry's standard dummy text ever since</div>
                            </div>
                        </div>
                        <div class="row m0 proName"><a href="#">Pacchi And Nakshi Haram With Uncut Diamonds</a></div>
                        <div class="row m0 proBuyBtn">
                            <a class="addToCart btn" href="#pqen" rel="modal:open">Product Enquiry</a>
                        </div>
                    </div>
                </div>
               
            </div>

        </div>
    </div>
</section>
<?php require_once("footer.php"); ?>