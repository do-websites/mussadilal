<?php 
class Musd_dynamic {
    public $title;
    public $description;
    public $keywords;
    
    public function __construct(){

    }
}
$msdata = new Musd_dynamic();
inputData($msdata);
?>
<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $msdata->title; ?></title>
    <meta name="description" content="<?php echo $msdata->description; ?>">
    <meta name="keywords" content="<?php echo $msdata->keywords; ?>">
    <meta name="author" content="Saiarlen">
    <!--Favicons-->
    <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <!--Bootstrap and Other Vendors-->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/fontawesome-all.min.css">
    <link rel="stylesheet" href="vendors/owl.carousel/css/owl.carousel.min.css">

    <link rel="stylesheet" type="text/css" href="vendors/flexslider/flexslider.css" media="screen" />
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Questrial" rel="stylesheet">
    <!--Mechanic Styles-->
    <link rel="stylesheet" href="css/jquery.modal.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/responsive.css">

    <!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
      <![endif]-->
    <style>
    #back-to-top.show {
        display: none !important;
    }
    </style>
</head>

<body>

<!-- Prelaoder -->
<div class="loader-wrapper">
    <div class="loader"></div>
    
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
  </div>
<!-- End of preloader -->
    <a href="#" id="back-to-top" title="Back to top">&uarr;</a>
    <header class="row mbg" id="header">
        <div class="row m0 logo_line ">
            <div class="container">
                <div class="row">
                    <div class="col-sm-5 logo">
                        <a href="index.php" class="logo_a"><img src="images/logou.png" alt="logo">
                        </a>
                    </div>
                    <div class="col-sm-7 searchSec">
                        <div class="col-sm-12">

                            <div class="fright wishlistCompare">
                                <div class="notifictxt">
                                    <p>Big Newyear Sale is On!</p>
                                </div>
                                <ul class="nav">
                                    <li>
                                        <a href="#">
                                            <span class="wish">
                                                <i class="fas fa-envelope"></i>
                                            </span>
                                            <span>info@musaddilal.co.in</span>
                                        </a>
                                    </li>
                                    <li class="h_cart">
                                        <a href="#">
                                            <span class="wish">
                                                <i class="fas fa-phone-square"></i>
                                            </span>
                                            <span>+91 9849044350</span>
                                        </a>

                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- <div class="col-sm-12">
                            <div class="fright searchForm">
                                <form action="#" method="get">
                                    <div class="input-group">
                                        <input type="hidden" name="search_param" value="all" id="search_param">
                                        <input type="search" class="form-control" placeholder="Search Products">
                                        <span class="input-group-btn searchIco">
                                 <button class="btn btn-default" type="submit"><i class="fas fa-search"></i></button>
                                 </span>
                                    </div>
                                    
                                </form>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
        <nav class="navbar navbar-default m0 navbar-static-top">
            <div class="container-fluid container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mainNav">
                        <i class="fas fa-bars"></i> MENU
                    </button>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="mainNav">
                    <ul class="nav navbar-nav">
                        <li id="home_act"><a href="index.php">Home</a></li>
                        <li id="about_act"><a href="about.php">About</a></li>
                        <li id="drop_act" class="mdropdown">
                            <!-- add  class mdropdown if dorpdown active -->
                            <a href="javascript:void(0);">Products
                                <i class="fa fa-chevron-down mmmob"></i>
                            </a>
                            <ul class="dropdown-menu level1">
                                <li class="dropdown-submenu">
                                    <a id="diam_act" href="product-diamond.php">Diamond </a>
                                </li>
                                <li class="dropdown-submenu">
                                    <a id="kun_act" href="product-kundan.php">Kundan</a>
                                </li>
                                <li class="dropdown-submenu">
                                    <a id="gold_act" href="product-gold.php">Gold
                                        <!--  <i class="fa fa-chevron-right amob"></i>
                                        <i class="fa fa-chevron-down bmob"></i> -->
                                    </a>
                                    <!-- <ul class="dropdown-menu sbm">
                                         <li><a id="gon" href="product-gold-necklace.php">Gold Necklace</a></li>
                                         <li><a id="gri" href="product-gold-ring.php">Gold Ring</a></li>
                                         <li><a id="gat" href="product-gold-antique.php">Antique/traditional</a></li>
                                    </ul> -->
                                </li>

                                <li class="dropdown-submenu">
                                    <a id="kun_act" href="product-polki.php">Polki</a>
                                </li>
                            </ul>
                        </li>
                        <!--  <li id="service_act"><a href="services.php">Services</a></li> -->
                       <!--  <li id="blogs_act"><a href="blogs.php">Blogs</a></li> -->
                        <li id="contact_act"><a href="contact.php">Contact Us</a></li>
                        <li class="offers_navbtn"><a href="#" data-appointlet-organization="musaddilal" data-appointlet-bookable="145430"><i class="fas fa-calendar-alt"></i>Book Appointments</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>
    </header>
    <script>


    </script>
    <!--Header-->