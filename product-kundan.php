<?php
require_once("header.php");
function inputData($msdata)
{
    $msdata-> title="Musadilal | Product";
    $msdata-> description="product description";
    $msdata-> keywords="product keywords";
}
?>
<script>
var element = document.getElementById("drop_act"); // only for dropdown menu items
element.classList.add("active");

/* var element = document.getElementById("diamond_act");
element.classList.add("active"); */
</script>

<section id="breadcrumbRow" class="row">
    <h2 style="background: url(images/kundan-hero.jpg) !important;">Kundans</h2>
    <div class="row pageTitle m0">
        <div class="container">
            <h4 class="fleft">kundans</h4>
            <ul class="breadcrumb fright">
                <li><a href="index.php">home</a></li>
                <li><a href="index.php">Products</a></li>
                <li class="active">kundans</li>
            </ul>
        </div>
    </div>
</section>

<section id="Musaddilal" class="row contentRowPad">
    <div class="container">
        <div class="row sectionTitle">
            <h2>Kundan Jewellery </h2>
            <h5>Our unique, custom-made kundan jewellery with antique finish are perfect for festivals, parties, and
                weddings.</h5>
        </div>
        <div class="row">
            <div class="col-sm-4 product">
                <div class="productInner row m0">
                    <div class="row m0 imgHov">
                        <img src="products/kundan/KUNDAN-NECKLACE-IN-POLKI-AND-PEARLS.jpg" alt="kundan">
                        <div class="row m0 hovArea">
                            <div class="row m0 proType">Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry. Lorem Ipsum has been the industry's standard dummy text ever since</div>
                        </div>
                    </div>
                    <div class="row m0 proName"><a href="#">Kundan Necklace In Polki And Pearls</a></div>
                    <div class="row m0 proBuyBtn">
                        <a class="addToCart btn" href="#pqen" rel="modal:open">Product Enquiry</a>
                    </div>
                </div>
            </div>
          

        </div>
    </div>
</section>
<?php require_once("footer.php"); ?>